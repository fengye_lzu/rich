package thoughtworks.rich.player;

import org.junit.Test;

import java.awt.*;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午3:36
 * To change this template use File | Settings | File Templates.
 */
public class PlayerTest {
    @Test
    public void shouldGetPlayerName() {
        Player player = new Player(PlayerName.孙小美, 10000);
        assertThat(player.getName(), is(PlayerName.孙小美));
    }
    @Test
    public void shouldGetPlayerMoney() {
        Player player = new Player(PlayerName.孙小美, 1000);
        assertThat(player.getMoney(), is(1000));
    }
    @Test
    public void shouldGetPlayerPoints() {
        Player player = new Player(PlayerName.孙小美, 1000);
        assertThat(player.getPoints() ,is(0));
    }
    @Test
    public void shouldAddPlayerMoney() {
        Player player = new Player(PlayerName.孙小美, 1000);
        player.gainMoney(100);
        assertThat(player.getMoney(), is(1100));
    }
    @Test
    public void shouldReducePlayerMoney() {
        Player player = new Player(PlayerName.孙小美, 1000);
        assertThat(player.reduceMoney(1100), equalTo(PlayerName.孙小美+"已破产"));
        assertThat(player.getMoney(), is(-100));
    }
    @Test
    public void shouldGetCurrentPosition() {
        Player player = new Player(PlayerName.孙小美, 1000);
        assertThat(player.getCurrentPosition(), is(0));
    }
    @Test
    public void getPlayerColor() {
        Player player = new Player(PlayerName.钱夫人, 1000);
        assertThat(player.getPlayerColor(), is(Color.RED));
    }
}
