package thoughtworks.rich.graph;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午8:30
 * To change this template use File | Settings | File Templates.
 */
public class MineGraphTest {
    @Test
    public void shouldGet$AtMineGraph() {
        Graph graph = new MineGraph(20);
        assertThat(graph.getName(), is('$'));
    }
    @Test
    public void shouldGetCorrectPointsAtMineGraph() {
        MineGraph graph = new MineGraph(20);
        assertThat(graph.getPoint(), is(20));
    }
}
