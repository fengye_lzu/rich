package thoughtworks.rich.graph;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午8:24
 * To change this template use File | Settings | File Templates.
 */
public class PrisonGraphTest {
    @Test
    public void shouldGetPAtPrisonGraph() {
        Graph graph = new PrisonGraph();
        assertThat(graph.getName(), is('P'));
    }
}
