package thoughtworks.rich.graph;

import org.junit.Test;
import thoughtworks.rich.graph.Graph;
import thoughtworks.rich.graph.StartGraph;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午4:14
 * To change this template use File | Settings | File Templates.
 */
public class StartGraphTest{
    @Test
    public void shouldGetSAtStartGraph() {
        Graph graph = new StartGraph();
        assertThat(graph.getName(), is('S'));
    }
}
