package thoughtworks.rich.graph;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午10:05
 * To change this template use File | Settings | File Templates.
 */
public class MagicRoomTest {
    @Test
    public void shouldGetMAtMagicRoom() {
        Graph graph = new MagicRoom();
        assertThat(graph.getName(), is('M'));
    }
}
