package thoughtworks.rich.graph;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午4:21
 * To change this template use File | Settings | File Templates.
 */
public class HospitalGraphTest {
    @Test
    public void shouldGetHAtHospitalGraph() {
        Graph graph = new HospitalGraph();
        assertThat(graph.getName(), is('H'));
    }
}
