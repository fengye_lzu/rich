package thoughtworks.rich.graph;

import org.junit.Test;
import thoughtworks.rich.graph.Graph;
import thoughtworks.rich.graph.PropsRoom;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午4:30
 * To change this template use File | Settings | File Templates.
 */
public class PropsRoomTest {
    @Test
    public void shouldGetTAtPropsRoom() {
        Graph graph = new PropsRoom();
        assertThat(graph.getName(), is('T'));
    }
    @Test
    public void shouldGetAllPropsAtPropsRoom() {
        PropsRoom graph = new PropsRoom();
        assertThat(graph.printProps(), equalTo("路障\t1\t50\t#\n机器娃娃\t2\t30\t \n炸弹\t3\t50\t@\n"));
    }
}
