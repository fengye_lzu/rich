package thoughtworks.rich.graph;



import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午5:28
 * To change this template use File | Settings | File Templates.
 */
public class GiftRoomTest {
    @Test
    public void shouldGetGAtGiftRoom() {
        Graph graph = new GiftRoom();
        assertThat(graph.getName(), is('G'));
    }
    @Test
    public void shouldGetAllGiftsAtGiftRoom() {
        GiftRoom graph = new GiftRoom();
        assertThat(graph.printGifts(), equalTo("奖金\t1\t2000\n点数卡\t2\t200\n福神\t3\t1\n"));
    }
}
