package thoughtworks.rich.graph;

import org.junit.Test;
import thoughtworks.rich.graph.SpaceGraph;
import thoughtworks.rich.player.Player;
import thoughtworks.rich.player.PlayerName;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午3:43
 * To change this template use File | Settings | File Templates.
 */
public class SpaceGraphTest {
    @Test
    public void getGraphName0AtSpace() {
        SpaceGraph graph = new SpaceGraph(200);
        assertThat(graph.getName(), is('0'));
    }
    @Test
    public void getGraphLevelAtSpace() {
        SpaceGraph graph = new SpaceGraph(200);
        assertThat(graph.getLevel(), is(0));
    }
    @Test
    public void getGraphPriceAtSpace() {
        SpaceGraph graph = new SpaceGraph(200);
        assertThat(graph.getPrice(), is(200));
    }
    @Test
    public void getGraphOwnerAtSpace() {
        SpaceGraph graph = new SpaceGraph(200);
        graph.setOwner(new Player(PlayerName.钱夫人, 1000));
        assertThat(graph.getOwner().getName(), is(PlayerName.钱夫人));
    }
}
