package thoughtworks.rich;

import org.hamcrest.Matcher;
import org.junit.Test;
import thoughtworks.rich.player.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午5:15
 * To change this template use File | Settings | File Templates.
 */
public class HomePageTest {
    @Test
    public void shouldGetCorrectPlayerList() {
        HomePage homePage = new HomePage();
        CopyOnWriteArrayList<Player> playerList = new CopyOnWriteArrayList<Player>();
        playerList = homePage.getPlayerList();
        for(Player player: playerList) {
            System.out.println(player.getName());
            System.out.println(player.getMoney());
            System.out.println(player.getPoints());
        }
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
