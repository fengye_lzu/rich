package thoughtworks.rich;

import org.junit.Test;
import thoughtworks.rich.extend.PlatChar;
import thoughtworks.rich.extend.Props;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.player.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午8:36
 * To change this template use File | Settings | File Templates.
 */
public class PlatTest {
    @Test
    public void shouldPrintCorrectPlat() {
        CopyOnWriteArrayList<Player> playerList = new CopyOnWriteArrayList<Player>();
        Plat plat = new Plat(playerList);
        ArrayList<PlatChar> platCharList = plat.getPlatCharList();
        for(PlatChar platChar: platCharList) {
            RichConsole.setTextAttributes(platChar.getCharColor(), platChar.getBackColor());
            System.out.print(platChar.getDiaplayChar());
        }
    }
    @Test
    public void shouldGetPlatCharSets() {
        char [] platCharSet;
        CopyOnWriteArrayList<Player> playerList = new CopyOnWriteArrayList<Player>();
        Plat plat = new Plat(playerList);
        ArrayList<PlatChar> playChars = plat.getPlatCharList();

    }
    @Test
    public void shouldGetAllPropsList() {
        HashMap<Integer, Props> propsMap = new HashMap<Integer, Props>();
        CopyOnWriteArrayList<Player> playerList = new CopyOnWriteArrayList<Player>();
        Plat plat = new Plat(playerList);
        plat.addProps(Props.BLOCK, 3);
        plat.addProps(Props.BLOCK, 3);
        plat.addProps(Props.BOMB, 3);
        plat.addProps(Props.BOMB, 5);
        propsMap = plat.getPropsMap();
        Set keySet = propsMap.keySet();
        Iterator keyIterator = keySet.iterator();
        while(keyIterator.hasNext()) {
            Integer index = (Integer) keyIterator.next();
            System.out.println(index);
            System.out.println(propsMap.get(index));

        }
        plat.addProps(Props.ROBOT, 2);
        propsMap = plat.getPropsMap();
        keySet = propsMap.keySet();
        keyIterator = keySet.iterator();
        while(keyIterator.hasNext()) {
            Integer index = (Integer) keyIterator.next();
            System.out.println(index);
            System.out.println(propsMap.get(index));
        }
    }
}
