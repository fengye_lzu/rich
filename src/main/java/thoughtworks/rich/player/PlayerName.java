package thoughtworks.rich.player;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午3:39
 * To change this template use File | Settings | File Templates.
 */
public enum PlayerName {
    钱夫人(Color.RED),阿土伯(Color.BLUE),孙小美(Color.YELLOW),金贝贝(Color.GREEN);
    private Color color;

    PlayerName(Color color) {
        this.color = color;
    }
    public Color getColor() {
        return color;
    }
}
