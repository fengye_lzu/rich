package thoughtworks.rich.player;

import thoughtworks.rich.*;
import thoughtworks.rich.extend.Props;
import thoughtworks.rich.extend.RichCommand;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.graph.SpaceGraph;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午3:42
 * To change this template use File | Settings | File Templates.
 */
public class Player {
    private PlayerName name;
    private int money;
    private int points;
    private static final int ZERO = 0;
    private int currentPosition;
    private Color playerColor;

    private HashMap<Integer, SpaceGraph> landListHashMap;
    private ArrayList<ArrayList<Props>> propsArrayList;
    private static final int BLOCK_INDEX = 0;
    private static final int BOMB_INDEX = 1;
    private static final int ROBOT_INDEX = 2;

    private final String LEGAL_CMD = "请输入合法命令";
    private int inHospitalDays = 0;
    private static final int PROPS_NUM_MAX = 10;
    private int billikenDays = 0;
    private int prisonDay;
    private boolean isInHospital = false;
    private boolean isInPrison = false;

    public Player(PlayerName name, int initMoney) {
        this.name = name;
        money = initMoney;
        points = 0;
        currentPosition = 0;
        playerColor = name.getColor();
        landListHashMap = new HashMap<Integer, SpaceGraph>();
        propsArrayList = new ArrayList<ArrayList<Props>>();
        propsArrayList.add(BLOCK_INDEX, new ArrayList<Props>());
        propsArrayList.add(BOMB_INDEX, new ArrayList<Props>());
        propsArrayList.add(ROBOT_INDEX, new ArrayList<Props>());
    }

    public PlayerName getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }

    public int getPoints() {
        return points;
    }

    public String gainMoney(int gainedMoney) {
        money += gainedMoney;
        return name+"获得"+gainedMoney+"元";
    }

    public String reduceMoney(int reducedMoney) {
        money -= reducedMoney;
        if(money<ZERO) return getName()+"已破产";
        return getName()+"还剩"+money+"元";
    }

    public boolean backrupted() {
        return money<=ZERO;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public Color getPlayerColor() {
        return playerColor;
    }

    public void gainLand(SpaceGraph spaceGraph) {
        landListHashMap.put(currentPosition, spaceGraph);
        System.out.println(name+"获得一块土地");
    }

    public String landInformation() {
        int spaceNum = 0, hutchNum = 0, houseNum = 0, skyscraperNum = 0;
        String spacePlace = "", hutchPlace = "", housePlace ="", skyscraperPlace = "";
        Set landKeySet = landListHashMap.keySet();
        Iterator landKeyIterator = landKeySet.iterator();
        SpaceGraph spaceGraph;
        int level, place;
        while(landKeyIterator.hasNext()) {
            place = (Integer)landKeyIterator.next();
            spaceGraph = landListHashMap.get(place);
            level = spaceGraph.getLevel();
            if(level==0) {spaceNum++; spacePlace = spacePlace+" "+place;}
            if(level==1) {hutchNum++; hutchPlace = hutchPlace+" "+place;}
            if(level==2) {houseNum++; housePlace = housePlace+" "+place;}
            if(level==3) {skyscraperNum++; skyscraperPlace = " "+place;}
        }
        return "Space "+spaceNum+" at "+spacePlace+";\n" +
                "Hutch "+hutchNum+" at " +hutchPlace+";\n" +
                "House "+houseNum+" at " +housePlace+";\n" +
                "Skyscraper "+skyscraperNum+" at "+skyscraperPlace+";\n";
    }

    public String propsInformation() {
        int blockNum=0, bombNum=0, robotNum=0;
        blockNum = propsArrayList.get(BLOCK_INDEX).size();
        bombNum = propsArrayList.get(BOMB_INDEX).size();
        robotNum = propsArrayList.get(ROBOT_INDEX).size();
        int blockStart=1, blockEnd=blockStart+blockNum-1;
        int bombStart=blockEnd+1, bombEnd=bombStart+bombNum-1;
        int robotStart=bombEnd+1, robotEnd=robotStart+robotNum-1;
        return "Block "+blockNum+"(1——"+blockEnd+");\tBomb "+bombNum+"("+bombStart+"——"+bombEnd+");\tRobot"+robotNum+"("+robotStart+"——"+robotEnd+");";
    }

    public void play(Controller controller) {
        RichConsole.setTextAttributes(playerColor);
        if(canPlay()) {
            System.out.println(name + ">");
            String cmd = (RichConsole.readLine()).trim().toLowerCase();
            while(!cmd.equals("roll")) {
                while(!RichCommand.cmdHashMap.containsKey(cmd.split("\\s")[0])){
                    System.out.println(LEGAL_CMD);
                    System.out.println(name + ">");
                    cmd = (RichConsole.readLine()).trim().toLowerCase();
                }
                if(!cmd.equals("roll")) {
                    controller.excute(this, cmd);
                    cmd = (RichConsole.readLine()).trim().toLowerCase();
                }
            }
            controller.excute(this, cmd);
        }
    }

    private boolean canPlay() {
        if(inHospitalDays>0) {
            System.out.println(name + " is in hospital");
            inHospitalDays--;
            System.out.println(inHospitalDays + " days left");
            return false;
        } else {
            isInHospital = false;
        }
        if(prisonDay>0) {
            System.out.println(name + " is in prison");
            prisonDay--;
            System.out.println(prisonDay + " days left");
            return false;
        } else {
            isInPrison = false;
        }
        return true;
    }

    public void setCurrentPosition(int position) {
        currentPosition = position;
    }

    public void toHospital(int position) {
        currentPosition = position;
        inHospitalDays = 3;
        isInHospital = true;
        System.out.println(name +"被送往医院");
    }

    public void gainPoints(int gainedPoints) {
        points += gainedPoints;
        System.out.println(name+"获得"+gainedPoints+"点卡");
    }

    public boolean reducePoints(int reducedPoints) {
        if(points-reducedPoints>=0) {
            points -= reducedPoints;
            return true;
        } else {
            return false;
        }
    }

    public void construct(SpaceGraph spaceGraph) {
        if(money>=spaceGraph.getPrice()) {
            reduceMoney(spaceGraph.getPrice());
            spaceGraph.upLevel();
        } else {
            System.out.println(name+"余额不足");
        }
    }

    public void breakup() {
        Set landKeySet = landListHashMap.keySet();
        Iterator landKeyIterator = landKeySet.iterator();
        SpaceGraph spaceGraph;
        int level;
        while(landKeyIterator.hasNext()) {
            spaceGraph = landListHashMap.get(landKeyIterator.next());
            spaceGraph.release();
        }
    }

    public void buyGraph(SpaceGraph spaceGraph) {
        if(money>=spaceGraph.getPrice()) {
            reduceMoney(spaceGraph.getPrice());
            spaceGraph.setOwner(this);
            gainLand(spaceGraph);
        } else {
            System.out.println(name+"余额不足");
        }
    }

    public void gainProps(Props props) {
        if(propsNum() >= PROPS_NUM_MAX) {
            System.out.println("最多拥有"+PROPS_NUM_MAX+"个道具");
            return ;
        }
        reducePoints(props.getPrice());
        if(props==Props.BLOCK) propsArrayList.get(BLOCK_INDEX).add(props);
        if(props==Props.BOMB) propsArrayList.get(BOMB_INDEX).add(props);
        if(props==Props.ROBOT) propsArrayList.get(ROBOT_INDEX).add(props);
    }

    private int propsNum() {
        int blockNum=0, bombNum=0, robotNum=0;
        blockNum = propsArrayList.get(BLOCK_INDEX).size();
        bombNum = propsArrayList.get(BOMB_INDEX).size();
        robotNum = propsArrayList.get(ROBOT_INDEX).size();
        return blockNum+bombNum+robotNum;
    }

    public int hasBilliken() {
        if(billikenDays>0) {
            billikenDays--;
            return billikenDays;
        }else {
            return 0;
        }
    }

    public void billikenEffect() {
        billikenDays = 5;
        System.out.println(name+"福神附身");
    }

    public void setPrisonDay(int prisonDay) {
        this.prisonDay = prisonDay;
        isInPrison = true;
    }

    public void sell(int index) {
         SpaceGraph spaceGraph = landListHashMap.get(new Integer(index));
         if(spaceGraph!=null) {
            landListHashMap.remove(new Integer(index));
            gainMoney(spaceGraph.sellPrice());
            spaceGraph.release();
         } else {
             System.out.println("地块不存在");
         }
    }

    public void sellTool(int index) {
        if(index>0) {
            if(index<propsNum()) {
                int blockNum=0, bombNum=0, robotNum=0;
                blockNum = propsArrayList.get(BLOCK_INDEX).size();
                bombNum = propsArrayList.get(BOMB_INDEX).size();
                if(index<blockNum) sellBlock();
                else if(index<blockNum+bombNum) sellBomb();
                else sellRobot();
                return;
            }
        }
        System.out.println("请输入正确道具编号");
    }

    private void sellRobot() {
        propsArrayList.get(ROBOT_INDEX).remove(0);
        gainPoints(Props.ROBOT.getPrice());
    }

    private void sellBomb() {
        propsArrayList.get(BOMB_INDEX).remove(0);
        gainPoints(Props.BOMB.getPrice());
    }

    private void sellBlock() {
        propsArrayList.get(BLOCK_INDEX).remove(0);
        gainPoints(Props.BLOCK.getPrice());
    }

    public boolean canUseBlock() {
        int blockNum = propsArrayList.get(BLOCK_INDEX).size();
        if(blockNum>0) {
            propsArrayList.get(BLOCK_INDEX).remove(0);
            return true;
        }
        return false;
    }

    public boolean canUserBomb() {
        int bombNum = propsArrayList.get(BOMB_INDEX).size();
        if(bombNum>0) {
            propsArrayList.get(BOMB_INDEX).remove(0);
            return true;
        }
        return false;
    }

    public boolean canUseRobot() {
        int robotNum = propsArrayList.get(ROBOT_INDEX).size();
        if(robotNum>0) {
            propsArrayList.get(ROBOT_INDEX).remove(0);
            return true;
        }
        return false;
    }

    public boolean isInHospital() {
        return isInHospital;
    }

    public boolean isInPrison() {
        return isInPrison;
    }
}
