package thoughtworks.rich.extend;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午9:34
 * To change this template use File | Settings | File Templates.
 */
public class PlatChar {
    private char diaplayChar;
    private Color charColor;
    private Color backColor;

    public PlatChar(char diaplayChar, Color charColor, Color backColor) {
        this.diaplayChar = diaplayChar;
        this.charColor = charColor;
        this.backColor = backColor;
    }

    public void setDiaplayChar(char diaplayChar) {
        this.diaplayChar = diaplayChar;
    }

    public void  setCharColor(Color color) {
        this.charColor = color;
    }

    public void setBackColor(Color color) {
        this.backColor = color;
    }

    public char getDiaplayChar() {
        return diaplayChar;
    }

    public Color getCharColor() {
        return charColor;
    }

    public Color getBackColor() {
        return backColor;
    }
}
