package thoughtworks.rich.extend;

import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-7
 * Time: 下午2:10
 * To change this template use File | Settings | File Templates.
 */
public class RichCommand {
    public static HashMap<String, String> cmdHashMap = new HashMap<String, String>();
    public static final String ROLL = "roll";
    static {
        cmdHashMap.put("roll", "掷骰子命令，行走1~6步");
        cmdHashMap.put("block", "玩家拥有路障后，可将路障放置到离当前位置前后10步的距离，\n任一玩家经过路障，都将被拦截。该道具一次有效。\nn:前后的相对距离，负数表示后方。\n");
        cmdHashMap.put("bomb", "可将路障放置到离当前位置前后10步的距离，任一玩家j 经过在该位置，\n将被炸伤，送往医院，住院三天。n 前后的相对距离，负数表示后方。");
        cmdHashMap.put("robot", "使用该道具，可清扫前方路面上10步以内的其它道具，如炸弹、路障。");
        cmdHashMap.put("sell", "出售自己的房产，x 地图上的绝对位置，即地产的编号。");
        cmdHashMap.put("selltool", "出售道具，x 道具编号");
        cmdHashMap.put("query", "显示自家资产信息");
        cmdHashMap.put("help", "查看命令帮助");
        cmdHashMap.put("quit", "强制退出");
    }
}
