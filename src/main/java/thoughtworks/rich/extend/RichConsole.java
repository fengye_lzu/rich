package thoughtworks.rich.extend;

import enigma.console.Console;
import enigma.console.TextAttributes;
import enigma.core.Enigma;
import thoughtworks.rich.player.Player;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午4:22
 * To change this template use File | Settings | File Templates.
 */
public class RichConsole {
    private static final Console console;
    public static final Color DEFAULT_CHAR_COLOR = Color.white;
    public static final Color DEFAULT_BACK_COLOR = Color.black;
    static
    {
        console = Enigma.getConsole("大富翁");
    }

    public static void setTextAttributes(Color frontColor) {
        TextAttributes attrs = new TextAttributes(frontColor);
        console.setTextAttributes(attrs);
    }

    public static void setTextAttributes(Color frontColor, Color backColor) {
        TextAttributes attrs = new TextAttributes(frontColor, backColor);
        console.setTextAttributes(attrs);
    }

    public static String readLine() {
        return console.readLine();
    }

    public static boolean isNumeric(String str)
    {
        Pattern pattern = Pattern.compile("-?[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if( !isNum.matches() )
        {
            return false;
        }
        return true;
    }
}
