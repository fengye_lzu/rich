package thoughtworks.rich.extend;

import thoughtworks.rich.player.Player;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午5:38
 * To change this template use File | Settings | File Templates.
 */
public enum Gift {
    BONUS("奖金",1,2000),
    POINTCARD("点数卡",2,200),
    BILLIKEN("福神",3,1);

    private int number;
    private String chName;
    private int amount;

    public String getChName() {
        return chName;
    }
    public int getNumber() {
        return number;
    }
    public int getAmount() {
        return amount;
    }

    public void effect(Player player) {
        if(this.equals(Gift.BONUS)) player.gainMoney(this.getAmount());
        if(this.equals(Gift.POINTCARD)) player.gainPoints(this.getAmount());
        if(this.equals(Gift.BILLIKEN)) player.billikenEffect();
    }

    Gift(String name, int number, int amount) {
        this.chName = name;
        this.number = number;
        this.amount = amount;
    }
}
