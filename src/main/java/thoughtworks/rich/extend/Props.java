package thoughtworks.rich.extend;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午4:55
 * To change this template use File | Settings | File Templates.
 */
public enum Props {
    BLOCK("路障",1,50,'#'),
    ROBOT("机器娃娃",2,30,' '),
    BOMB("炸弹",3,50,'@');

    private int number;
    private int price;
    private String chName;
    private char sign;

    public String getChName() {
        return chName;
    }
    public int getNumber() {
        return number;
    }
    public int getPrice() {
        return price;
    }
    public char getSign() {
        return sign;
    }

    Props(String name, int number, int price, char sign) {
        this.chName = name;
        this.number = number;
        this.price = price;
        this.sign = sign;
    }
}
