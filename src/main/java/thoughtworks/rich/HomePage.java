package thoughtworks.rich;

import thoughtworks.rich.extend.PlatChar;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.player.Player;
import thoughtworks.rich.player.PlayerName;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午4:13
 * To change this template use File | Settings | File Templates.
 */
public class HomePage {
    private static CopyOnWriteArrayList<Player> playerList;
    private static ArrayList<Player> faiedPlayerList;
    private static int initMoney;
    private static String cmd;
    private static final int DEFAULT_MONEY = 10000;
    private static final int MAX_MNOEY = 50000;
    private static final int MIN_MONEY = 1000;
    private static final String INPUT_MONEY = "设置玩家初始资金，范围1000～50000,quit退出";
    private static final String INPUT_CORRECT_NUM = "请输入正确的数字，quit退出";
    private static final String INPUT_PLAYER = "请选择2~4位不重复玩家，输入编号即可。(1.钱夫人; 2.阿土伯; 3.孙小美; 4.金贝贝，quit退出):";
    private static final PlayerName[] PLAYER_NAME_ARRAY = {PlayerName.钱夫人,PlayerName.阿土伯,PlayerName.孙小美,PlayerName.金贝贝};
    private static final String GAME_START = "游戏开始";
    private static Plat plat;
    private static final String QUIT = "quit";
    private static final int ASCLLOF0 = 48;
    private static final int ZERO = 0;
    private static Controller controller;

    static
    {
        playerList = new CopyOnWriteArrayList<Player>();
        faiedPlayerList = new ArrayList<Player>();
        initMoney = DEFAULT_MONEY;
        cmd = "";
        RichConsole.setTextAttributes(RichConsole.DEFAULT_CHAR_COLOR);
    }

    public HomePage() {
        inputInitMoney();
        while(initMoney<MIN_MONEY||initMoney>MAX_MNOEY) inputInitMoney();
        inputPlayers();
        startGameInfo();
        initPlat();
        printPlat();
        initController();
        startGame();
    }

    private void initController() {
        controller = new Controller(plat);
    }

    private void startGame() {
       while(playerList.size()>1) {
            for(Player player:playerList) {
                if(playerList.size()==1) break;
                player.play(controller);
                printPlat();
            }
       }
       System.out.println(playerList.get(0).getName()+"获胜");
    }

    public static void printPlat() {
        ArrayList<PlatChar> platCharList = plat.getPlatCharList();
        for(PlatChar platChar: platCharList) {
            RichConsole.setTextAttributes(platChar.getCharColor(), platChar.getBackColor());
            System.out.print(platChar.getDiaplayChar());
        }
    }

    private void startGameInfo() {
        System.out.println(GAME_START);
    }

    private void inputInitMoney() {
        System.out.println(INPUT_MONEY);
        while(!RichConsole.isNumeric(cmd=RichConsole.readLine())) {
            System.out.println(INPUT_CORRECT_NUM);
            if(cmd.equals(QUIT))  System.exit(0);
        }
        initMoney = Integer.parseInt(cmd);
    }

    private void inputPlayers() {
        System.out.println(INPUT_PLAYER);
        while(!RichConsole.isNumeric(cmd=RichConsole.readLine())||!verifiedInputPlayerName(cmd)) {
            System.out.println(INPUT_PLAYER);
            if(cmd.equals(QUIT))  System.exit(0);
        }
        parsePlayer(cmd);
    }

    private void parsePlayer(String cmd) {
        for(int index=0; index<cmd.length(); index++) {
            playerList.add(new Player(PLAYER_NAME_ARRAY[cmd.charAt(index)- ASCLLOF0 -1], initMoney));
        }
    }

    private boolean verifiedInputPlayerName(String cmd) {
        if(cmd.length()>PLAYER_NAME_ARRAY.length) return false;
        for(int index=0; index<cmd.length(); index++) {
            if(cmd.charAt(index)<'1'||cmd.charAt(index)>'4') return false;
        }
        for(int i=0; i<cmd.length(); i++) {
            for(int j=i+1; j<cmd.length(); j++) {
                if(cmd.charAt(i)==cmd.charAt(j)) return false;
            }
        }
        return true;
    }

    public static CopyOnWriteArrayList<Player> getPlayerList() {
        return playerList;
    }

    private  void initPlat() {
        plat = new Plat(playerList);
    }
}
