package thoughtworks.rich;

import thoughtworks.rich.dice.Dice;
import thoughtworks.rich.extend.Props;
import thoughtworks.rich.extend.RichCommand;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.graph.*;
import thoughtworks.rich.player.Player;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午9:21
 * To change this template use File | Settings | File Templates.
 */
public class Controller {
    private Plat plat;

    public Controller(Plat plat) {
        this.plat = plat;
    }

    public void excute(Player player, String cmd) {
        String []cmdSplit = cmd.split("\\s");
        if(cmdSplit[0].equals("query")) query(player);
        else if(cmdSplit[0].equals("help")) help();
        else if(cmdSplit[0].equals("quit")) quit();
        else if(cmdSplit[0].equals("roll")) roll(player);
        else if(cmdSplit[0].equals("block")&&cmdSplit.length==2) block(player, cmdSplit[1]);
        else if(cmdSplit[0].equals("bomb")&&cmdSplit.length==2) bomb(player, cmdSplit[1]);
        else if(cmdSplit[0].equals("robot")) robot(player);
        else if(cmdSplit[0].equals("sell")) sell(player);
        else if(cmdSplit[0].equals("selltool")) sellTool(player);
    }

    private void sellTool(Player player) {
        System.out.println(player.propsInformation());
        System.out.println("请选择要出售的道具编号,f结束");
        String cmd = RichConsole.readLine().trim().toLowerCase();
        while(!RichConsole.isNumeric(cmd)&&!cmd.equals("f")) {
            System.out.println("请选择要出售的道具编号,f结束");
            cmd = RichConsole.readLine().trim().toLowerCase();
        }
        if(RichConsole.isNumeric(cmd)) player.sellTool(Integer.parseInt(cmd));
    }

    private void sell(Player player) {
        System.out.println(player.landInformation());
        System.out.println("请选择要出售的地块,f结束");
        String cmd = RichConsole.readLine().trim().toLowerCase();
        while(!RichConsole.isNumeric(cmd)&&!cmd.equals("f")) {
            System.out.println("请选择要出售的地块，f结束");
            cmd = RichConsole.readLine().trim().toLowerCase();
        }
        if(RichConsole.isNumeric(cmd)) player.sell(Integer.parseInt(cmd));
    }

    private void robot(Player player) {
        if(player.canUseRobot()) plat.addProps(Props.ROBOT, player.getCurrentPosition());
        else System.out.println("没有机器人");
    }

    private void bomb(Player player, String s) {
       if(player.canUserBomb()){
           if(RichConsole.isNumeric(s)) {
               int step = Integer.parseInt(s);
               if(-10<step&&step<10) {
                   plat.addProps(Props.BOMB, player.getCurrentPosition()+step);
                   HomePage.printPlat();
                   return;
               }
           }
           System.out.println("请输入正确格式");
       } else {
           System.out.println("没有炸弹");
       }
    }

    private void block(Player player, String s) {
        if(player.canUseBlock()) {
            if(RichConsole.isNumeric(s)) {
                int step = Integer.parseInt(s);
                if(-10<step&&step<10) {
                    plat.addProps(Props.BLOCK, player.getCurrentPosition()+step);
                    HomePage.printPlat();
                    return;
                }
            }
            System.out.println("请输入正确格式");
        }else {
            System.out.println("没有路障");
        }
    }

    private void roll(Player player) {
        int step = Dice.roll();
        System.out.println(player.getName()+"掷"+step+"点");
        int nowPosition = player.getCurrentPosition();
        HashMap<Integer, Props> propsInPlat = plat.getPropsMap();
        for(int i=1; i<=step; i++) {
            if(propsInPlat.get(nowPosition+i)!=null) {
                if(propsInPlat.get(nowPosition+i)==Props.BOMB ) { plat.propsEffected(nowPosition+i); player.toHospital(plat.getHospitalPostion()); return;}
                if(propsInPlat.get(nowPosition+i)==Props.BLOCK) { plat.propsEffected(nowPosition+i); step = i;}
            }
        }
        playerGo(player, step);
        gainOrLoose(player);
    }

    private void gainOrLoose(Player player) {
        char sign = ((Graph)plat.getGraphList().get(player.getCurrentPosition())).getName();
        if(sign == 'T') inPropsRoom(player);
        else if(sign == 'G') inGiftRoom(player);
        else if(sign == 'P') inPrison(player);
        else if(sign == 'M') inMagicRoom(player);
        else if(sign == '$') gainPoints(player);
        else if(sign != 'S'&& sign != 'H') landBussiness(player);
    }

    private void landBussiness(Player player) {
        SpaceGraph spaceGraph = (SpaceGraph) plat.getGraphList().get(player.getCurrentPosition());
        if(spaceGraph.getOwner()==null) tackGraph(player, spaceGraph);
        else if(spaceGraph.getOwner()==player) constructGraph(player, spaceGraph);
        else penalty(player, spaceGraph);
    }

    private void penalty(Player player, SpaceGraph spaceGraph) {
        Player hostPlayer = spaceGraph.getOwner();
        if(player.hasBilliken()>0) {
            System.out.println(player.getName()+"福神附身，免过路费");
            return;
        }
        if(!hostPlayer.isInHospital()&&!hostPlayer.isInPrison()) {
            int money=0;
            if(player.getMoney()<spaceGraph.rentPrice())  money = player.getMoney();
            else money = spaceGraph.rentPrice();
            System.out.println(hostPlayer.gainMoney(money));
            System.out.println( player.reduceMoney(money));
            if(player.getMoney()<=0) breakup(player);
        } else {
            System.out.println(hostPlayer.getName()+"不在，免过路费");
        }
    }

    private void breakup(Player player) {
        HomePage.getPlayerList().remove(player);
        player.breakup();
        System.out.println(player.getName()+"破产了");
        player = null;
    }

    private void constructGraph(Player player, SpaceGraph spaceGraph) {
        if(spaceGraph.getLevel() < SpaceGraph.TOPEST_LEVEL) {
            System.out.println("是否升级该处地产，"+spaceGraph.getPrice()+"元（Y/N）?");
            String cmd = RichConsole.readLine().trim().toLowerCase();
            while(!cmd.equals("y")&&!cmd.equals("n")) {
                System.out.println("是否升级该处地产，"+spaceGraph.getPrice()+"元（Y/N）?");
                cmd = RichConsole.readLine().trim().toLowerCase();
            }
            if(cmd.equals("y")) player.construct(spaceGraph);

        }
    }

    private void tackGraph(Player player, SpaceGraph spaceGraph) {
        System.out.println("是否购买该处空地，"+spaceGraph.getPrice()+"元（Y/N）?");
        String cmd = RichConsole.readLine().trim().toLowerCase();
        while(!cmd.equals("y")&&!cmd.equals("n")) {
            System.out.println("是否购买该处空地，"+spaceGraph.getPrice()+"元（Y/N）?");
            cmd = RichConsole.readLine().trim().toLowerCase();
        }
        if(cmd.equals("y")) player.buyGraph(spaceGraph);
    }

    private void gainPoints(Player player) {
        MineGraph mineGraph = (MineGraph) plat.getGraphList().get(player.getCurrentPosition());
        player.gainPoints(mineGraph.getPoint());
        System.out.println(player.getName()+"获得"+mineGraph.getPoint()+"点");
    }

    private void inMagicRoom(Player player) {
        System.out.println(player.getName()+"进入魔法屋");
        MagicRoom magicRoom = (MagicRoom) plat.getGraphList().get(player.getCurrentPosition());
        magicRoom.playerComes(player);
    }

    private void inPrison(Player player) {
        System.out.println(player.getName()+"进入监狱");
        PrisonGraph prisonGraph = (PrisonGraph) plat.getGraphList().get(player.getCurrentPosition());
        prisonGraph.playerComes(player);
    }

    private void inGiftRoom(Player player) {
        System.out.println(player.getName()+"进入礼品屋");
        GiftRoom giftRoom = (GiftRoom) plat.getGraphList().get(player.getCurrentPosition());
        giftRoom.playerComes(player);
    }

    private void inPropsRoom(Player player) {
        System.out.println(player.getName()+"进入道具屋");
        PropsRoom propsRoom = (PropsRoom) plat.getGraphList().get(player.getCurrentPosition());
        propsRoom.clientComes(player);
    }

    private void playerGo(Player player, int step) {
        int newPosition = player.getCurrentPosition()+step;
        newPosition = newPosition%plat.getPlatLength();
        player.setCurrentPosition(newPosition);
    }

    private void quit() {
        System.exit(0);
    }

    private void help() {
        Iterator cmdMapKeyIterator = RichCommand.cmdHashMap.keySet().iterator();
        String cmdString;
        while(cmdMapKeyIterator.hasNext()) {
            cmdString = (String) cmdMapKeyIterator.next();
            System.out.print(cmdString+"\t");
            System.out.println(RichCommand.cmdHashMap.get(cmdString));
        }
    }

    private void query(Player player) {
        System.out.println(player.getMoney());
        System.out.println(player.getPoints());
        System.out.println(player.landInformation());
        System.out.println(player.propsInformation());
    }
}
