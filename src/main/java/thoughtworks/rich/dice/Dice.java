package thoughtworks.rich.dice;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-6
 * Time: 下午8:20
 * To change this template use File | Settings | File Templates.
 */
public class Dice {
    private static final int MAX_OF_DICE = 6;
    private static final Random random = new Random();
    public static int roll() {
        return random.nextInt(MAX_OF_DICE)+1;
    }
}
