package thoughtworks.rich;

import thoughtworks.rich.extend.PlatChar;
import thoughtworks.rich.extend.Props;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.graph.*;
import thoughtworks.rich.player.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午9:10
 * To change this template use File | Settings | File Templates.
 */
public class Plat {
    private ArrayList graphList;
    private HashMap<Integer,Props> propsMap;
    private ArrayList <PlatChar> platCharList;
    private static final int SECTION_1_LEGTH = 13;
    private static final int SECTION_1_PRICE = 200;
    private static final int SECTION_2_LEGTH = 13;
    private static final int SECTION_2_PRICE = 200;
    private static final int SECTION_3_LEGTH = 6;
    private static final int SECTION_3_PRICE = 500;
    private static final int SECTION_4_LEGTH = 13;
    private static final int SECTION_4_PRICE = 300;
    private static final int SECTION_5_LEGTH = 13;
    private static final int SECTION_5_PRICE = 300;
    private static final int MINE_LENGTH = 6;
    private StringBuffer graphStringBuffer;
    private static final int START_LENGTH = 1;
    private static final int HOSPITAL_LENGTH = 1;
    private static final int PROPS_ROOM_LENGTH = 1;
    private static final int WIDTH = 29;
    private int graphCurrenIndex;
    private static final int ROBOT_DISTANCE = 10;
    private static final char BLANK_CHAR = ' ';
    private CopyOnWriteArrayList<Player> playerList;
    private static final char NEW_LINE = '\n';
    private static final int HOSPITAL_POSTION = SECTION_1_LEGTH+START_LENGTH;
    private static int PLAT_LENGTH;

    public Plat(CopyOnWriteArrayList<Player> playerList) {
        graphList = new ArrayList();
        graphList.clear();
        addStart(graphList);
        addSection1(graphList);
        addHospital(graphList);
        addSection2(graphList);
        addPropsRoom(graphList);
        addSection3(graphList);
        addGiftRoom(graphList);
        addSection4(graphList);
        addPrison(graphList);
        addSection5(graphList);
        addMagicRoom(graphList);
        addMineGraph(graphList);
        this.playerList = playerList;
        graphStringBuffer = new StringBuffer();
        propsMap = new HashMap<Integer, Props>();
        platCharList = new ArrayList<PlatChar>();
        convertPlatToChar();
        PLAT_LENGTH = graphList.size();
    }

    private void convertPlatToChar() {
        graphCurrenIndex = 0;
        platCharList.clear();
        convertLine1();
        convertLine2();
        convertLine3();
        convertLine4();
        convertLine5();
        convertLine6();
        convertLine7();
        convertLine8();
    }

    private void convertLine8() {
        graphCurrenIndex = graphList.size()-7;
        char displayChar;
        Color charColor;
        for(int offSet=0; offSet<START_LENGTH+SECTION_1_LEGTH+HOSPITAL_LENGTH+SECTION_2_LEGTH+PROPS_ROOM_LENGTH; offSet++) {
            displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
            charColor = RichConsole.DEFAULT_CHAR_COLOR;
            platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
            graphCurrenIndex--;
        }
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine7() {
        char displayChar = ((Graph)graphList.get(graphList.size()-6)).getName();
        platCharList.add(constructPlatChar(displayChar, graphList.size()-6));
        for(int offSet=0; offSet<WIDTH-2; offSet++) {
            platCharList.add(new PlatChar(BLANK_CHAR, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
        }
        displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
        platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
        graphCurrenIndex++;
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine6() {
        char displayChar = ((Graph)graphList.get(graphList.size()-5)).getName();
        platCharList.add(constructPlatChar(displayChar, graphList.size()-5));
        for(int offSet=0; offSet<WIDTH-2; offSet++) {
            platCharList.add(new PlatChar(BLANK_CHAR, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
        }
        displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
        platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
        graphCurrenIndex++;
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine5() {
        char displayChar = ((Graph)graphList.get(graphList.size()-4)).getName();
        platCharList.add(constructPlatChar(displayChar, graphList.size()-4));
        for(int offSet=0; offSet<WIDTH-2; offSet++) {
            platCharList.add(new PlatChar(BLANK_CHAR, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
        }
        displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
        platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
        graphCurrenIndex++;
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine4() {
        char displayChar = ((Graph)graphList.get(graphList.size()-3)).getName();
        platCharList.add(constructPlatChar(displayChar, graphList.size()-3));
        for(int offSet=0; offSet<WIDTH-2; offSet++) {
            platCharList.add(new PlatChar(BLANK_CHAR, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
        }
        displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
        platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
        graphCurrenIndex++;
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine3() {
        char displayChar = ((Graph)graphList.get(graphList.size()-2)).getName();
        platCharList.add(constructPlatChar(displayChar, graphList.size()-2));
        for(int offSet=0; offSet<WIDTH-2; offSet++) {
            platCharList.add(new PlatChar(BLANK_CHAR, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
        }
        displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
        platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
        graphCurrenIndex++;
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine2() {
        char displayChar = ((Graph)graphList.get(graphList.size()-1)).getName();
        platCharList.add(constructPlatChar(displayChar, graphList.size()-1));
        for(int offSet=0; offSet<WIDTH-2; offSet++) {
            platCharList.add(new PlatChar(BLANK_CHAR, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
        }
        displayChar = ((Graph)graphList.get(graphCurrenIndex)).getName();
        platCharList.add(constructPlatChar(displayChar, graphCurrenIndex));
        graphCurrenIndex++;
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private void convertLine1() {
        char displayChar;
        Color charColor;
        int offSet;
        for(offSet=0; offSet<START_LENGTH+SECTION_1_LEGTH+HOSPITAL_LENGTH+SECTION_2_LEGTH+PROPS_ROOM_LENGTH; offSet++){
            displayChar = ((Graph)graphList.get(offSet)).getName();
            charColor = RichConsole.DEFAULT_CHAR_COLOR;
            platCharList.add(constructPlatChar(displayChar,  offSet));
            graphCurrenIndex++;
        }
        platCharList.add(new PlatChar(NEW_LINE, RichConsole.DEFAULT_CHAR_COLOR, RichConsole.DEFAULT_BACK_COLOR));
    }

    private PlatChar constructPlatChar(char displayChar, int index) {
        PlatChar platChar;
        Color backColor = RichConsole.DEFAULT_BACK_COLOR;
        Color charColor = RichConsole.DEFAULT_CHAR_COLOR;
        if(displayChar == '0'||displayChar=='1'||displayChar=='2'||displayChar=='3') {
            Player graphOwer = ((SpaceGraph)graphList.get(index)).getOwner();
            if(graphOwer!=null) charColor = graphOwer.getPlayerColor();
        }
        if(propsMap.get(index) != null) {
            displayChar = propsMap.get(index).getSign();
        }
        if(playerList!=null) {
            for(Player player: playerList) {
                if(index == player.getCurrentPosition()) {
                    backColor = player.getPlayerColor();
                }
            }
        }
        return new PlatChar(displayChar, charColor, backColor);
    }

    private void addStart(ArrayList list) {
        list.add(new StartGraph());
    }
    private void addSection1(ArrayList list) {
        for(int offSet = 0; offSet<SECTION_1_LEGTH; offSet++) {
            list.add(new SpaceGraph(SECTION_1_PRICE));
        }
    }
    private void addHospital(ArrayList list) {
        list.add(new HospitalGraph());
    }
    private void addSection2(ArrayList list) {
        for(int offSet = 0; offSet<SECTION_2_LEGTH; offSet++) {
            list.add(new SpaceGraph(SECTION_2_PRICE));
        }
    }
    private void addPropsRoom(ArrayList list) {
        list.add(new PropsRoom());
    }
    private void addSection3(ArrayList list) {
        for(int offSet = 0; offSet<SECTION_3_LEGTH; offSet++) {
            list.add(new SpaceGraph(SECTION_3_PRICE));
        }
    }
    private void addGiftRoom(ArrayList list) {
        list.add(new GiftRoom());
    }
    private void addSection4(ArrayList list) {
        for(int offSet = 0; offSet<SECTION_4_LEGTH; offSet++) {
            list.add(new SpaceGraph(SECTION_4_PRICE));
        }
    }
    private void addPrison(ArrayList list) {
        list.add(new PrisonGraph());
    }
    private void addSection5(ArrayList list) {
        for(int offSet = 0; offSet<SECTION_5_LEGTH; offSet++) {
            list.add(new SpaceGraph(SECTION_5_PRICE));
        }
    }
    private void addMagicRoom(ArrayList list) {
        list.add(new MagicRoom());
    }
    private void addMineGraph(ArrayList list) {
        list.add(new MineGraph(20));
        list.add(new MineGraph(80));
        list.add(new MineGraph(100));
        list.add(new MineGraph(40));
        list.add(new MineGraph(80));
        list.add(new MineGraph(60));

    }

    public HashMap<Integer,Props> getPropsMap() {
        return propsMap;
    }

    public void propsEffected(int position) {
        propsMap.remove(position);
    }

    public boolean addProps(Props props, int position) {
        if(props == Props.ROBOT) {
            for(int i=1; i<=ROBOT_DISTANCE; i++)
                propsMap.remove(position+i);
            return true;
        }
        if((propsMap.get(position))==null) {
            propsMap.put(position, props);
            return true;
        } else {
            System.out.println("此处已有道具");
        }
        return false;
    }

    public ArrayList<PlatChar> getPlatCharList() {
        convertPlatToChar();
        return platCharList;
    }

    public int getHospitalPostion() {
        return HOSPITAL_POSTION;
    }

    public int getPlatLength() {
        return PLAT_LENGTH;
    }

    public ArrayList getGraphList() {
        return graphList;
    }
}
