package thoughtworks.rich.graph;

import thoughtworks.rich.extend.Props;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.player.Player;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午4:32
 * To change this template use File | Settings | File Templates.
 */
public class PropsRoom extends Graph {

    private ArrayList<Props> propsList;
    private static final String WELCOM_STRING = "欢迎光临道具屋， 请选择您所需要的道具：\n" +
            "道具\t编号\t价值（点数）\t显示方式\n";
    private static final int CHEAPEST_POINT = 30;

    public PropsRoom() {
        super('T');
        propsList = new ArrayList<Props>();
        propsList.add(Props.BLOCK);
        propsList.add(Props.ROBOT);
        propsList.add(Props.BOMB);
    }

    public String printProps() {
        StringBuffer propsInfo = new StringBuffer();
        for(Props prop : propsList) {
            propsInfo.append(prop.getChName()+"\t");
            propsInfo.append(prop.getNumber()+"\t");
            propsInfo.append(prop.getPrice()+"\t");
            propsInfo.append(prop.getSign()+"\n");
        }
        return propsInfo.toString();
    }

    public void playerComeIn() {

    }

    public void clientComes(Player player) {
        System.out.print(WELCOM_STRING+printProps());
        String cmd = RichConsole.readLine().trim().toLowerCase();
        while(!cmd.equals("f")) {
            if(player.getPoints()<CHEAPEST_POINT) {
                System.out.println("点数不够");
                break;
            }
            if(RichConsole.isNumeric(cmd)) {
                int index = Integer.parseInt(cmd);
                if(0<index&&propsList.size()>=index) {
                    sellProps(player, index);
                    break;
                }
            }
            System.out.println("请输入正确编号，f退出");
            cmd = RichConsole.readLine().trim().toLowerCase();
        }
    }

    private void sellProps(Player player, int index) {
        Props props = propsList.get(index-1);
        System.out.println("您将购买"+props.getChName()+",将耗费"+props.getPrice()+",是否继续(Y/N)?");
        String cmd = RichConsole.readLine().trim().toLowerCase();
        while(!cmd.equals("y")&&!cmd.equals("n")&&!cmd.equals("f")) {
             System.out.println("请输入正确选择,f退出");
             cmd = RichConsole.readLine().trim().toLowerCase();
        }
        if(cmd.equals("y")) player.gainProps(props);
    }
}
