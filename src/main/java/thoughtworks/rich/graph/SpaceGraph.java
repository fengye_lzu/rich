package thoughtworks.rich.graph;

import thoughtworks.rich.player.Player;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午3:51
 * To change this template use File | Settings | File Templates.
 */
public class SpaceGraph extends Graph{
    private int level;
    private int price;
    private Player owner=null;
    public static final int TOPEST_LEVEL = 3;

    public SpaceGraph(int price) {
        super('0');
        level = 0;
        this.price = price;
    }

    public int getLevel() {
        return level;
    }

    public int getPrice() {
        return price;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public void upLevel() {
        if(level<TOPEST_LEVEL) {
            level++;
            name++;
        }
    }

    public void playerComeIn() {

    }

    public void setName(char name) {
        this.name = name;
    }

    public int rentPrice() {
        int rentPrice = price/2;
        for(int i=0; i<level; i++) {
            rentPrice *= 2;
         }
        return rentPrice;
    }

    public int sellPrice() {
        int sellPrice = price;
        for(int i=0; i<level; i++) {
            sellPrice *= 2;
        }
        return sellPrice*2;
    }

    public void release() {
        setOwner(null);
        setName('0');
    }
}
