package thoughtworks.rich.graph;

import thoughtworks.rich.player.Player;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午8:27
 * To change this template use File | Settings | File Templates.
 */
public class PrisonGraph extends Graph {
    public PrisonGraph() {
        super('P');
    }

    public void playerComes(Player player) {
        player.setPrisonDay(2);
    }
}
