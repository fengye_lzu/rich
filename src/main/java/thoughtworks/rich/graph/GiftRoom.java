package thoughtworks.rich.graph;

import thoughtworks.rich.extend.Gift;
import thoughtworks.rich.extend.RichConsole;
import thoughtworks.rich.player.Player;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: zgq
 * Date: 13-3-5
 * Time: 下午5:30
 * To change this template use File | Settings | File Templates.
 */
public class GiftRoom extends Graph {

    private static final String WELCOM_STRING = "欢迎光临礼品屋，请选择一件您 喜欢的礼品：\n" +
            "礼品\t编号\n";
    private ArrayList<Gift> giftList;
    public GiftRoom() {
        super('G');
        giftList = new ArrayList<Gift>();
        giftList.add(Gift.BONUS);
        giftList.add(Gift.POINTCARD);
        giftList.add(Gift.BILLIKEN);
    }

    public String printGifts() {
        StringBuffer giftsInfo = new StringBuffer();
        for(Gift gift : giftList) {
            giftsInfo.append(gift.getChName()+"\t");
            giftsInfo.append(gift.getNumber()+"\t");
            giftsInfo.append(gift.getAmount()+"\n");
        }
        return giftsInfo.toString();
    }

    public void playerComes(Player player) {
        System.out.print(WELCOM_STRING + printGifts());
        String cmd = RichConsole.readLine().trim().toLowerCase();
        if(RichConsole.isNumeric(cmd)) {
            int index = Integer.parseInt(cmd);
            if(index>0&&index<=giftList.size())
                giftList.get(index-1).effect(player);
        }
    }
}
